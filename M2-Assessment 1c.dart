// Class to hold app name information
class AppPrint {  
   
   // String to hold the  variables 
   String appName = "EasyEquities"; 
   String appCategory = "Best Consumer Solution Category" ;
   String appDeveloper ="First World Trader";
   String appYearWin = "2020";
  

   // A method/function to print out the variables 
   void printAppName() { 
      print(appName);
      print(appCategory);
      print(appDeveloper);
      print(appYearWin);
   } 
  // function to convert appName to uppercase 
   void appNametoCase() {
           print (appName.toUpperCase());
   }

}


void main() { 
  
   //Creating the object by istatiating an instance of the class AppPrint
   AppPrint printName = AppPrint();
     // Accesing the printAppName function inside the object and print output
     printName.printAppName();
     printName.appNametoCase();
    
    }
  

