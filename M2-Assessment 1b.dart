void main(List<String> args) {


  // Capture the winning app for each year in a array for that particular year, First letter must be capitalised for sort() to work   
  List<String> mtnAppofTheYearWinners = [];
  List<String> mtnAppofTheYear2012 = ["FNB Banking","Health ID","Transunion Dealer Guide","Rapid Targets","Matchy","Plascon","PhraZapp" ];
  List<String> mtnAppofTheYear2013 = ["DStv",".comm Telco Data Vizualiser","PriceCheck Mobile","MarkitShare","Nedbank App Suite","SnapScan","Kids Aid","Bookly","Gautrain Buddy"];
  List<String> mtnAppofTheYear2014 = ["Live Inspect", "Vigo","Zapper","Rea Vaya","Wildlife Tracker","SuperSport","SyncMobile","My Belongings"];
  List<String> mtnAppofTheYear2015 = ["Wum Drop","DSTV","Vula Mobile","CPUT Mobile","EskomSePuch","M4JAM"];
  List<String> mtnAppofTheYear2016 = ["Domestly","IKhokha","HearZA","Tuta-me","KaChing","Friendly Math Monsters for Kindergarten"];
  List<String> mtnAppofTheYear2017 = ["Shyft","TransUnion 1Check","OrderIN","EcoSlips","InterGreatMe","Zulzi","Hey Jude","Oru Social","TouchSA","Pick n Pay Super Animals 2","The TreeApp South Africa","WatIf Health Portal","Awethu Project"];
  List<String> mtnAppofTheYear2018 = ["Pineapple","Cowa Bunga","Digemy Knowledge Partner and Besmarter","Bestee","The African Cyber Gaming League App (ACGL)","DBTrack","Stockfella","Difela Hymns","Xander English 1-20","Ctrl","Khula","ASI Snakes"];
  List<String> mtnAppofTheYear2019 = ["Naked","SI Realities","Lost Defence","Franc","Vula Mobile","Matric Live","My Pregnancy Journal and LocTransie","Hydra","Bottles","Over","Digger","Mo Wash"];
  List<String> mtnAppofTheYear2020 = ["Easy Equities","Examsta","Checkers Sixty60","Technishen","BirdPro","Lexie Hearing","GreenFingers Mobile","Xitsonga Dictionary","StokFella ","Bottles","Phanda Ispani"];
  List<String> mtnAppofTheYear2021 = ["Ambani Africa","Takealot","Shyft","IIDENTIFii","SiSa","Guardian Health","Murimi","UniWise","Kazi App","Rekindle Learning","Afrihost","Hello Pay"];
 


    //Create one array/list with all the winners so we can sort and arrange as needed
    mtnAppofTheYearWinners.addAll(mtnAppofTheYear2012 + mtnAppofTheYear2013 + mtnAppofTheYear2014 + mtnAppofTheYear2015 + mtnAppofTheYear2016 + mtnAppofTheYear2017 + mtnAppofTheYear2018 + mtnAppofTheYear2019 + mtnAppofTheYear2020 + mtnAppofTheYear2021);

    //Sort the array/list alphabetically, Will work as long as all names are captured with first letter capitalised. 
    mtnAppofTheYearWinners.sort();
    
      // Print out the array of all the winners
      print(mtnAppofTheYearWinners);

      //Print outthe winning apps for 2017and 2018
       print("Winnning app of 2017 is " + mtnAppofTheYear2017[0]);
       print("Winnning app of 2017 is " + mtnAppofTheYear2018[0]);

       //Print out the total number of apps from the array
       print(mtnAppofTheYearWinners.length);


}
